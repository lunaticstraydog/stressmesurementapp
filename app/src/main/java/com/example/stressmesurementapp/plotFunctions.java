package com.example.stressmesurementapp;

import android.content.Context;

import com.androidplot.ui.HorizontalPositioning;
import com.androidplot.ui.VerticalPosition;
import com.androidplot.ui.VerticalPositioning;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.CatmullRomInterpolator;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PanZoom;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYGraphWidget;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class plotFunctions {
    public static  XYPlot getPlot(XYPlot plot, List<MesurementRecord> mesurementRecords, int N,MesurementItem currentItem, Context context,List<Number> critValues,List<String> dateTitles) //N max number of items to show
    {
        plot.clear();

        int size = critValues.size();
        if (size>N) size=N;
        Number[][] dataXY=getDataXY(size,critValues);
        String[] dateLabels= new String[size];
        for (int i = dateTitles.size()-size ;i < dateTitles.size(); i++)
        {
            try
            {
                dateLabels[i-(dateTitles.size()-size)] = shortDate(dateTitles.get(i));
            }
            catch (java.text.ParseException dateUnreadable)
            {
                dateLabels[i] = "date";
            }

        }
        XYSeries series1 = new SimpleXYSeries(Arrays.asList(dataXY[1]), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,currentItem.getText());
        plot.setRangeBoundaries(0,11, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.SUBDIVIDE,12);
        plot.setDomainStep(StepMode.SUBDIVIDE,Math.min(size,12));
        plot.getLegend().position(20,
                HorizontalPositioning.ABSOLUTE_FROM_LEFT,
                70,
                VerticalPositioning.ABSOLUTE_FROM_BOTTOM);
        plot.setTitle("Historique du critère");
        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(context, R.xml.line_point_formatter_with_labels_blue);
        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/
        if (size >= 3)
            series1Format.setInterpolationParams(
                    new CatmullRomInterpolator.Params(100, CatmullRomInterpolator.Type.Centripetal));
        // add a new series' to the xyplot:
        //PanZoom.attach(plot);
        plot.addSeries(series1, series1Format);
        plot.getGraph().setDomainGridLinePaint(null);
        plot.getGraph().setRangeGridLinePaint(null);
        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                return toAppendTo.append(dateLabels[i]);
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
        plot.redraw();
        return plot;
    }


    public static  List<Number> getTotalPlot(XYPlot plot, List<MesurementRecord> mesurementRecords, int N, Context context,List<Number> totalValues,List<String> dateTitles) //N max number of items to show
    {
        plot.clear();

        int size = totalValues.size();
        if (size>N) size=N;
        Number[][] dataXY=getDataXY(size,totalValues);
        String[] dateLabels= new String[size];
        for (int i = dateTitles.size()-size ;i < dateTitles.size(); i++)
        {
            try
            {
                dateLabels[i-(dateTitles.size()-size)] = shortDate(dateTitles.get(i));
            }
            catch (java.text.ParseException dateUnreadable)
            {
                dateLabels[i] = "date";
            }

        }

        XYSeries series1 = new SimpleXYSeries(Arrays.asList(dataXY[1]), SimpleXYSeries.ArrayFormat.Y_VALS_ONLY,"Score total");
        plot.setRangeBoundaries(0,11, BoundaryMode.FIXED);
        plot.setRangeStep(StepMode.SUBDIVIDE,12);
        plot.setDomainStep(StepMode.SUBDIVIDE,Math.min(size,12));

        //PanZoom panZoom= PanZoom.attach(plot);


        LineAndPointFormatter series1Format =
                new LineAndPointFormatter(context, R.xml.line_point_formatter_with_labels_blue);
        // just for fun, add some smoothing to the lines:
        // see: http://androidplot.com/smooth-curves-and-androidplot/

        if (size >= 3)
            series1Format.setInterpolationParams(
                new CatmullRomInterpolator.Params(100, CatmullRomInterpolator.Type.Centripetal));
        // add a new series' to the xyplot:
        plot.setTitle("Historique des résultats");
        plot.addSeries(series1, series1Format);
        plot.getGraph().setDomainGridLinePaint(null);
        plot.getGraph().setRangeGridLinePaint(null);
        plot.getGraph().getLineLabelStyle(XYGraphWidget.Edge.BOTTOM).setFormat(new Format() {
            @Override
            public StringBuffer format(Object obj, StringBuffer toAppendTo, FieldPosition pos) {
                int i = Math.round(((Number) obj).floatValue());
                return toAppendTo.append(dateLabels[i]);
            }
            @Override
            public Object parseObject(String source, ParsePosition pos) {
                return null;
            }
        });
        plot.redraw();
        return totalValues;
    }






    public static Number[][] getDataXY(Integer N, List<Number> critValues) // return last N records
    {
        Number[][] dataXY = new Number[2][N];
        for(int i =critValues.size() - N ; i <= critValues.size()-1; i++)
        {
            int pos=i-(critValues.size()-N);
            dataXY[0][pos]=i;
            dataXY[1][pos]=critValues.get(i);
        };
        return dataXY;
    }

    public static String shortDate(String longDate) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("EEEE d MMMM , HH:mm:s ", Locale.getDefault());
        Date date = df.parse(longDate);
        SimpleDateFormat shortDateFormat = new SimpleDateFormat("d MMM", Locale.getDefault());
        String shortDate= shortDateFormat.format(date);
        return shortDate;
    }


    public static void getCritsAndDate(List<MesurementRecord> mesurementRecords, MesurementItem currentItem,List<Number> critValues, List<String> dateTitles,int N)
    {
        for (MesurementRecord mesurementRecord : mesurementRecords) {
            MesurementItem critToAdd = mesurementRecord.getMesurements().get(currentItem.getId());
            if (critToAdd != null) {
                Double toAdd = critToAdd.getValue() / critToAdd.getMaxValue() * 10;
                BigDecimal score = new BigDecimal(Double.toString(toAdd)).setScale(1, RoundingMode.HALF_UP);
                critValues.add(score.doubleValue());
                dateTitles.add(mesurementRecord.getTitle());
            }
        }
    }
    public static void getTotalsAndDate(List<MesurementRecord> mesurementRecords,List<Number> totalValues, List<String> dateTitles,int N)
    {
        for (MesurementRecord mesurementRecord : mesurementRecords)
        {
                double totalScore = 0;
                double totalPond = 0;
            for (Map.Entry<String, MesurementItem> entry : mesurementRecord.getMesurements().entrySet()) {
                String key = entry.getKey();
                MesurementItem item = entry.getValue();
                totalScore += (item.getValue() / item.getMaxValue()) * 10 * item.getPonderation();
                totalPond += item.getPonderation();
            }
            BigDecimal score = new BigDecimal(totalScore / totalPond).setScale(1, RoundingMode.HALF_UP);
            totalValues.add(score);
            dateTitles.add(mesurementRecord.getTitle());
        }
    }
}
