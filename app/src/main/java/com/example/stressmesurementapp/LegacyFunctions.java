package com.example.stressmesurementapp;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


public class LegacyFunctions {

    private static final String TUPLE_KEY = "tuple_key_-10";
    private static final String LEGACY_KEY = "legacy_key_104";

    public static class StringValueTuple <K,V> {

        public  K string;
        public  V value;

        public List<String> title;

        public StringValueTuple(K string, V value,List<String> title){
            this.string = string;
            this.value = value;
            this.title=title;
        }

        public  StringValueTuple(K string, V value){
            this.string = string;
            this.value = value;
            this.title=new ArrayList<>();
            List<List<String>> listStrings= (List<List<String>>) string;
            for (List<String> str : listStrings)
            {
                this.title.add("placeholder title");
            }
        }
    }

    public static StringValueTuple readStringValueListInPref(Context context)
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String jsonStringValueList=pref.getString(TUPLE_KEY, "");
        System.out.println(jsonStringValueList);
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        //Gson gson = new Gson();
        Type type_tuple=new TypeToken<StringValueTuple>() {}.getType();
        StringValueTuple stringValueTuple=gson.fromJson(jsonStringValueList,type_tuple );
        return stringValueTuple;
    }


    public static List<MesurementRecord> convertLegacyList(List<MesurementRecord> mesurementRecords, Context context, TextView mainText, SeekBar seekBar)
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String is_legacy = pref.getString(LEGACY_KEY,"");
        if (mesurementRecords == null)
            mesurementRecords= new  ArrayList<>();
        int oldSize=mesurementRecords.size();
        if (is_legacy != "")
        {
            return mesurementRecords;
        }
        StringValueTuple stringValueTuple = readStringValueListInPref(context);
        System.out.println(stringValueTuple.value);
        if (stringValueTuple.string == null)
        {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString(LEGACY_KEY, "done");
            editor.apply();
            Toast.makeText(context,
                    "Pas d'enregistrements a convertir",
                    Toast.LENGTH_LONG).show();
            return mesurementRecords;
        }

        for (int i=0; i< ((List<List<String>>)stringValueTuple.string).size(); i++ )
        {
            List<String> itemStrings=((List<List<String>>)stringValueTuple.string).get(i);
            String itemTitle= (String) stringValueTuple.title.get(i);
            List<Double> itemValues=((List<List<Double>>)stringValueTuple.value).get(i);
            LinkedHashMap<String,MesurementItem> itemList= new LinkedHashMap<>();
            for (int j=0; j<itemStrings.size(); j++)
            {
                MesurementItem mesurementItem = new MesurementItem(itemStrings.get(j),mainText,seekBar,itemValues.get(j),1.0,10);
                itemList.put(itemStrings.get(j),mesurementItem);
            }
                MesurementRecord oldRecord = new MesurementRecord(itemList,itemTitle);
            mesurementRecords.add(i,oldRecord); //to keep ordering the same
        }
        Toast.makeText(context,
                "Converti "+ String.valueOf(mesurementRecords.size() - oldSize)+" enregistrements. Nombre d'enregistrements a présent : "+String.valueOf(mesurementRecords.size()),
                Toast.LENGTH_LONG).show();

        SharedPreferences.Editor editor = pref.edit();
        editor.putString(LEGACY_KEY, "done");
        editor.apply();
        return mesurementRecords;

    }
}
