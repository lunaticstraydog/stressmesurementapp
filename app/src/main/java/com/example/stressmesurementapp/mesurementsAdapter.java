package com.example.stressmesurementapp;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.content.Context;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYSeries;
import com.androidplot.xy.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;


import static com.example.stressmesurementapp.PrefConfig.readMesurementItemListInPref;
import static com.example.stressmesurementapp.plotFunctions.getCritsAndDate;
import static com.example.stressmesurementapp.plotFunctions.getPlot;
import static com.example.stressmesurementapp.plotFunctions.getTotalPlot;
import static com.example.stressmesurementapp.updateMainText.updateMainText;


import static java.security.AccessController.getContext;

import androidx.annotation.NonNull;

import com.google.android.material.slider.Slider;
//import com.travijuu.numberpicker.library.NumberPicker;
import java.util.ArrayList;
import java.util.List;

public class mesurementsAdapter extends BaseAdapter {

    private Context context;
    private LinkedHashMap<String,MesurementItem> elementsList;
    private LayoutInflater inflater;

    private List<MesurementRecord> mesurementRecords;

    public String m_Text;
    public mesurementsAdapter(Context context , LinkedHashMap<String,MesurementItem> elementsList, List<MesurementRecord> mesurementRecords)
    {
        this.context=context;
        this.elementsList=elementsList;
        this.inflater=LayoutInflater.from(context);
        this.mesurementRecords=mesurementRecords;
    }

    @Override
    public int getCount() {
        return elementsList.size();
    }

    @Override
    public MesurementItem getItem(int i) {
        Set<String> keySet = elementsList.keySet();
        String[] keyArray
                = keySet.toArray(new String[keySet.size()]);
        String key = keyArray[i ]; //-1
        return elementsList.get(key);

    }

    public void removeItem(int i)
    {
        Set<String> keySet = elementsList.keySet();
        String[] keyArray
                = keySet.toArray(new String[keySet.size()]);
        String key = keyArray[i];
        elementsList.remove(key);
    }
    public LinkedHashMap<String,MesurementItem> getItemList() {
        return elementsList;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }




    @Override
    public View getView(int itemId, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.adapter_item,null);
        MesurementItem currentItem = getItem(itemId);
        String text=currentItem.getText();
        TextView textToUpdate= view.findViewById(R.id.criteria);
        textToUpdate.setText(text);
        TextView valueToUpdate= view.findViewById(R.id.value);
        valueToUpdate.setText(String.valueOf(getItem(itemId).getValue())+"/"+String.valueOf(getItem(itemId).getMaxValue()));
        Button deleteButton= view.findViewById(R.id.delete_button);
        Button editButton= view.findViewById(R.id.edit_button);
        ImageButton settingsButton = view.findViewById(R.id.settings) ;
        ImageButton chartButton = view.findViewById(R.id.graph) ;
        com.google.android.material.slider.Slider slider =view.findViewById(R.id.slider);
        slider.setValue((float)getItem(itemId).getValue());
        slider.setValueTo((float)getItem(itemId).getMaxValue());
        TextView mainText=getItem(itemId).getMainText();
        SeekBar seekbar=getItem(itemId).getSeekBar();
        editButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("EditWindow");
                final EditText input = new EditText(context);

                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setText(currentItem.getText());
                builder.setView(input);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_Text = input.getText().toString();
                        getItem(itemId).setText(m_Text);
                        notifyDataSetChanged();
                        List <String> stringList= new ArrayList<>();
                        for (Map.Entry<String,MesurementItem> entry : getItemList().entrySet() )
                        {
                            String key = entry.getKey();
                            MesurementItem item = entry.getValue();
                            stringList.add(item.getText());
                        }
                        PrefConfig.writeListInPref(context, stringList);
                    }
                });
                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener()
        {
        public void onClick(View view)
        {
            new AlertDialog.Builder(context)
                    .setTitle("Souhaitez vous enlever \"" + getItem(itemId).getText()+"\" ?")
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    removeItem(itemId);
                                    notifyDataSetChanged();
                                    List <String> stringList= new ArrayList<>();
                                    for (Map.Entry<String,MesurementItem> entry : getItemList().entrySet() )
                                    {
                                        String key = entry.getKey();
                                        MesurementItem item = entry.getValue();
                                        stringList.add(item.getText());
                                    }
                                    PrefConfig.writeListInPref(context, stringList);
                                }
                            }).setNegativeButton("Non", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
        }
        });

        slider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                getItem(itemId).setValue(value);
                valueToUpdate.setText(String.valueOf(getItem(itemId).getValue())+"/"+String.valueOf(getItem(itemId).getMaxValue()));
                updateMainText(elementsList,mainText,seekbar);
            }
        });

        settingsButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                View viewPopupWindow = inflater.inflate(R.layout.settingspopupwindowlayout,null);
                PopupWindow popupWindow = new PopupWindow(viewPopupWindow,900,ViewGroup.LayoutParams.WRAP_CONTENT,true);
                popupWindow.setAnimationStyle(R.style.Animation);
                popupWindow.showAtLocation(view, Gravity.CENTER,0,0);
                EditText ponderationField= viewPopupWindow.findViewById(R.id.ponderation_field);
                Slider ponderationSlider= viewPopupWindow.findViewById(R.id.ponderation_slider);
                int maxPondValue=10;
                int maxMaxValue=100;
                ponderationSlider.setValueFrom(0);
                ponderationSlider.setValueTo(maxPondValue);

                ponderationField.setText(String.valueOf(getItem(itemId).getPonderation()));
                ponderationSlider.setValue(Math.min((float) getItem(itemId).getPonderation(),maxPondValue));

                EditText maxValueField= viewPopupWindow.findViewById(R.id.max_value_field);
                Slider maxValueSlider= viewPopupWindow.findViewById(R.id.val_max_slider);
                maxValueSlider.setValueFrom(1);
                maxValueSlider.setValueTo(maxMaxValue);

                maxValueSlider.setValue(Math.min((float) getItem(itemId).getMaxValue(),maxMaxValue));
                maxValueField.setText(String.valueOf(getItem(itemId).getMaxValue()));

                Button saveButton= viewPopupWindow.findViewById(R.id.save_params);
                Button cancelButton= viewPopupWindow.findViewById(R.id.cancel_param);

                ponderationField.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) { }
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                    @Override
                    public void afterTextChanged(Editable s) {
                        try
                        {
                            float pondValue = Float.parseFloat(ponderationField.getText().toString());
                            ponderationSlider.setValue(Math.min( maxPondValue,pondValue));
                            if (pondValue > maxPondValue)
                            {
                                Toast.makeText(context,
                                        "La valeur enregistrée dépasse du slider, mais sera enregistrée correctement",
                                        Toast.LENGTH_LONG).show();
                                ponderationSlider.setValue(maxPondValue);
                            }
                            if (pondValue < 0)
                            {
                                ponderationSlider.setValue(0);
                                ponderationField.setText(String.valueOf(0));
                                Toast.makeText(context,
                                        "Valeur inférieure a zéro, remplacée par 0.",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception E)
                        {
                            ponderationSlider.setValue(0);
                            ponderationField.setText(String.valueOf(0));
                        }
                    }
                });

                ponderationSlider.addOnChangeListener(new Slider.OnChangeListener() {
                    @Override
                    public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                        if (fromUser) ponderationField.setText(String.valueOf(value));
                    }
                });



                maxValueField.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) { }
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
                    @Override
                    public void afterTextChanged(Editable s) {
                        try {
                            float maxValue = Float.parseFloat(maxValueField.getText().toString());
                            maxValueSlider.setValue(Math.min(maxMaxValue, maxValue));
                            if (maxValue > maxMaxValue) {
                                Toast.makeText(context,
                                        "La valeur enregistrée dépasse du slider, mais sera enregistrée correctement",
                                        Toast.LENGTH_LONG).show();
                                maxValueSlider.setValue(maxMaxValue);
                            }
                            if (maxValue < 1) {
                                maxValueSlider.setValue(1);
                                maxValueField.setText(String.valueOf(1));
                                Toast.makeText(context,
                                        "Valeur inférieure a un, remplacée par 0.",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                        catch (Exception E)
                        {
                            maxValueSlider.setValue(0);
                            maxValueField.setText(String.valueOf(0));
                        }

                    }
                });
                maxValueSlider.addOnChangeListener(new Slider.OnChangeListener() {
                    @Override
                    public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                        if(fromUser) maxValueField.setText(String.valueOf(value));
                    }
                });



                cancelButton.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        popupWindow.dismiss();

                    }
                });

                saveButton.setOnClickListener(new View.OnClickListener()
                {
                    public void onClick(View view)
                    {
                        String ponderationValue = ponderationField.getText().toString();
                        String maxValue = maxValueField.getText().toString();
                        getItem(itemId).setPonderation(Double.parseDouble(ponderationValue));
                        int maxValueRound= Math.round(Float.parseFloat(maxValue));
                        getItem(itemId).setMaxValue(maxValueRound);
                        if (getItem(itemId).getValue() >  getItem(itemId).getMaxValue()) getItem(itemId).setValue(getItem(itemId).getMaxValue());
                        slider.setValue((float) getItem(itemId).getValue());
                        slider.setValueTo(getItem(itemId).getMaxValue());
                        valueToUpdate.setText(String.valueOf(getItem(itemId).getValue())+"/"+String.valueOf(getItem(itemId).getMaxValue()));
                        updateMainText(elementsList,mainText,seekbar);
                        popupWindow.dismiss();
                    }
                });

            }
        });


        chartButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                List<Number> critValues= new ArrayList<>();
                List<String>  dateTitles = new ArrayList<>();
                int N=10;
                getCritsAndDate(mesurementRecords, currentItem,critValues,dateTitles, N);
                if (critValues.size() <= 1)
                {
                    Toast.makeText(context, "Il faut au moins 2 enregistrements avec ce critère pour afficher un historique !", Toast.LENGTH_LONG).show();
                }
                else {
                    View viewPopupWindow = inflater.inflate(R.layout.chartpopuplayout, null);
                    int height = Resources.getSystem().getDisplayMetrics().heightPixels;
                    int width = Resources.getSystem().getDisplayMetrics().widthPixels;
                    PopupWindow popupWindow = new PopupWindow(viewPopupWindow, width * 95 / 100,ViewGroup.LayoutParams.WRAP_CONTENT, true);
                    popupWindow.setOutsideTouchable(false);
                    popupWindow.setAnimationStyle(R.style.Animation);
                    popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

                    // initialize our XYPlot reference:

                    XYPlot plot = (XYPlot) viewPopupWindow.findViewById(R.id.history_chart);

                    getPlot(plot, mesurementRecords, N, getItem(itemId), context, critValues, dateTitles);


                    Slider daySlider = (Slider) viewPopupWindow.findViewById(R.id.days_slider);
                    if (critValues.size() == 2)
                        {daySlider.setVisibility(view.GONE);}
                    else
                    {
                        daySlider.setValue(Math.min(N, critValues.size()));
                        daySlider.setValueTo(critValues.size());
                        daySlider.setValueFrom(2);
                        daySlider.addOnChangeListener(new Slider.OnChangeListener() {
                            @Override
                            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                                int valueInt = Math.round(value);
                                getPlot(plot, mesurementRecords, valueInt, getItem(itemId), context, critValues, dateTitles);

                            }
                        });
                    }


                }
            }
        });


                return view;
    }


}
