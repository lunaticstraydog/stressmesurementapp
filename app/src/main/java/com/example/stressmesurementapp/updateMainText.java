package com.example.stressmesurementapp;

import android.widget.SeekBar;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class updateMainText {
    public static void updateMainText(LinkedHashMap<String,MesurementItem> itemList, TextView mainText, SeekBar seekBar)
    {
        double totalScore=0;
        double totalPond=0;

        for (Map.Entry<String,MesurementItem> entry : itemList.entrySet() )
        {
            String key = entry.getKey();
            MesurementItem item = entry.getValue();
            totalScore+=(item.getValue()/item.getMaxValue())*10*item.getPonderation();
            totalPond+=item.getPonderation();
        }

        BigDecimal score = new BigDecimal(totalScore/totalPond).setScale(1, RoundingMode.HALF_UP);
        mainText.setText("Total : "+String.valueOf(score)+"/10");
        seekBar.setProgress(score.intValue());
    }
}
