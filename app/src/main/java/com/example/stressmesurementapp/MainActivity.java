package com.example.stressmesurementapp;

import static com.example.stressmesurementapp.PrefConfig.readMesurementItemListInPref;
import static com.example.stressmesurementapp.PrefConfig.writeMesurementItemListInPref;
import static com.example.stressmesurementapp.plotFunctions.getPlot;
import static com.example.stressmesurementapp.plotFunctions.getTotalPlot;
import static com.example.stressmesurementapp.plotFunctions.getTotalsAndDate;
import static com.example.stressmesurementapp.LegacyFunctions.convertLegacyList;
import static com.example.stressmesurementapp.updateMainText.updateMainText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidplot.xy.XYPlot;
import com.google.android.material.slider.Slider;
import com.opencsv.CSVWriter;

import android.Manifest.permission.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private String m_Text = "";
    private Context context=this;
    private LinkedHashMap<String,MesurementItem> itemList;

    private int currentDay=0;

    private ListView elementsListView;
    private Button ajoutItemButton ;
    private Button save_day ;
    private Button previous_day;
    private Button next_day;

    private ImageButton total_chart;
    private TextView title;
    private  Button deleteTitleButton;
    private mesurementsAdapter mesurements_adapter;
    private TextView mainText;
    private SeekBar seekbar;
    private List<String> stringList;



    public List<MesurementRecord> mesurementRecords;

    private ImageButton exportCsv;

    private Button CSV_texte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        itemList=new LinkedHashMap<>();
        LayoutInflater inflater = getLayoutInflater();
        mainText= (TextView)findViewById(R.id.total);
        seekbar = findViewById(R.id.seekbar);


        elementsListView= findViewById(R.id.elements_list);
        ajoutItemButton = findViewById(R.id.ajoutItem);
        save_day = findViewById(R.id.save_day);
        previous_day= findViewById(R.id.previous_day);
        next_day= findViewById(R.id.next_day);
        total_chart=findViewById(R.id.graph_total);

        title= findViewById(R.id.title);
        deleteTitleButton= findViewById(R.id.title_delete_button);
        exportCsv=findViewById(R.id.export_csv);
        CSV_texte=findViewById(R.id.csv_texte);
        title.setText(getDateString());



        mesurementRecords = readMesurementItemListInPref(context);
        mesurements_adapter = new mesurementsAdapter(this, itemList,mesurementRecords);
        elementsListView.setAdapter(mesurements_adapter);
        stringList=PrefConfig.readListInPref(this) ;

        mesurementRecords=convertLegacyList(mesurementRecords,context,mainText,seekbar);
        writeMesurementItemListInPref(context,mesurementRecords);



        //initialistion of the list
        if (stringList == null) {stringList= Arrays.asList(getResources().getStringArray(R.array.default_criterias)); PrefConfig.writeListInPref(getApplicationContext(), stringList);}

        if (mesurementRecords == null || mesurementRecords.size() == 0 )
        {
            currentDay=0; // on commence avec rien
            for (String string : stringList)
            {
                itemList.put(string,new MesurementItem(string,mainText,seekbar));
            }
        }
        else
        {
            currentDay = mesurementRecords.size()  - 1 ; // par convention les jours démarrent a zéro et finissent a n-1

            restoreDay(mesurementRecords, currentDay);
        }
        updateButtonVisibility(next_day,previous_day,deleteTitleButton,mesurementRecords);



        ajoutItemButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Title");

                final EditText input = new EditText(context);
                input.setInputType(InputType.TYPE_CLASS_TEXT );
                builder.setView(input);

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        m_Text = input.getText().toString();
                        itemList.put(m_Text,new MesurementItem(m_Text,mainText,seekbar));
                        List <String> stringList= new ArrayList<>();

                        for (Map.Entry<String,MesurementItem> entry : itemList.entrySet() )
                        {
                            String key = entry.getKey();
                            MesurementItem item = entry.getValue();
                            stringList.add(0,item.getText());
                        }

                        PrefConfig.writeListInPref(getApplicationContext(), stringList);
                        mesurements_adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();

            }
        });
        save_day.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                title.setText(getDateString());
                List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                if (mesurementRecords==null)  mesurementRecords= new ArrayList<>();

                MesurementRecord mesurementRecord=new MesurementRecord(itemList,title.getText().toString());
                mesurementRecords.add(mesurementRecord);
                writeMesurementItemListInPref(context,mesurementRecords);

                if (mesurementRecords.size() == 1) // on vient de sauvegarder avec une taille de 1 -> on veut pouvoir revenir en arrière
                {
                    currentDay=1;
                }
                else currentDay =mesurementRecords.size() - 1 ;
                updateButtonVisibility(next_day,previous_day,deleteTitleButton,mesurementRecords);
            }
        });

        previous_day.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {

                List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                if (mesurementRecords == null || currentDay == 0)
                {
                    Toast.makeText(getApplicationContext(),
                            "Il n'y a pas d'enregistrements avant celui-ci",
                            Toast.LENGTH_LONG).show();
                    previous_day.setAlpha(0.5F);
                    previous_day.setClickable(false);

                }
                else
                {
                    Iterator<String> it = itemList.keySet().iterator();
                    while (it.hasNext()) {
                        it.next();
                        it.remove();
                    }

                    restoreDay(mesurementRecords,currentDay-1);
                    currentDay--;
                    System.out.println("Current day = " + currentDay);
                    System.out.println("len liste = " +mesurementRecords.size() );

                }
                updateButtonVisibility(next_day,previous_day,deleteTitleButton,mesurementRecords);
            }
        });
        next_day.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                if (mesurementRecords == null || currentDay >= mesurementRecords.size() - 1 )
                {
                    Toast.makeText(getApplicationContext(),
                            "Il n'y a pas d'enregistrements après celui-ci",
                            Toast.LENGTH_LONG).show();
                }
                else
                {
                    restoreDay(mesurementRecords, currentDay + 1);
                    currentDay++;

                    System.out.println("Current day = " + currentDay);
                    System.out.println("len liste = " +mesurementRecords.size() );
                }
                updateButtonVisibility(next_day,previous_day,deleteTitleButton,mesurementRecords);
            }
        });

        deleteTitleButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                if (mesurementRecords !=null && mesurementRecords.size() > 0)
                {
                    new AlertDialog.Builder(context)
                            .setTitle("Souhaitez vous enlever l'enregistrement \"" + title.getText() + "\" ?")
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    int indexToDelete=currentDay;
                                    if (mesurementRecords.size() == 1) indexToDelete=0;
                                    mesurementRecords.remove(indexToDelete);
                                    writeMesurementItemListInPref(context,mesurementRecords);
                                    int elementsLeft = mesurementRecords.size();

                                    if (elementsLeft == 0)
                                    {
                                        stringList=PrefConfig.readListInPref(context) ;

                                        if (stringList == null)
                                        {
                                            stringList= Arrays.asList(getResources().getStringArray(R.array.default_criterias));
                                        }
                                        PrefConfig.writeListInPref(getApplicationContext(), stringList);


                                        Iterator<String> it = itemList.keySet().iterator();
                                        while (it.hasNext()) {
                                            it.next();
                                            it.remove();
                                        }


                                        for (int strnum = 0; strnum < stringList.size(); strnum++)
                                        {
                                            itemList.put(stringList.get(strnum),new MesurementItem(stringList.get(strnum),mainText,seekbar));
                                            //itemList.get(strnum).setValue(0);
                                        }

                                        mainText.setText("Total : 0/10");
                                        seekbar.setProgress(0);
                                        title.setText(getDateString());
                                        mesurements_adapter.notifyDataSetChanged();
                                        currentDay=0;
                                    }
                                    else
                                    {
                                        if (elementsLeft == 1)
                                        {
                                            restoreDay(mesurementRecords,0);
                                            currentDay=0;
                                        }
                                        else if (currentDay != 0)
                                        {
                                            restoreDay(mesurementRecords,currentDay-1);
                                            currentDay--;
                                        }
                                        else
                                        {
                                            restoreDay(mesurementRecords,currentDay);
                                            //currentDay++;
                                        }
                                    }

                                    updateButtonVisibility(next_day,previous_day,deleteTitleButton,mesurementRecords);

                                }
                            }).setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).create().show();
                }
                else  Toast.makeText(getApplicationContext(),
                        "Il n'y a pas d'enregistrements a supprimer",
                        Toast.LENGTH_LONG).show();

            }
        });


        total_chart.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                int N=10;
                List<Number> totalValues= new ArrayList<>();
                List<String> dateTitles= new ArrayList<>();
                if (mesurementRecords != null)  getTotalsAndDate( mesurementRecords,totalValues, dateTitles,N);
                if (totalValues.size() <= 1 || mesurementRecords == null)
                {
                    Toast.makeText(context, "Il faut au moins 2 enregistrements avec ce critère pour afficher un historique !", Toast.LENGTH_LONG).show();
                }
                else
                {
                    View viewPopupWindow = inflater.inflate(R.layout.chartpopuplayout, null);
                    int height = Resources.getSystem().getDisplayMetrics().heightPixels;
                    int width = Resources.getSystem().getDisplayMetrics().widthPixels;
                    PopupWindow popupWindow = new PopupWindow(viewPopupWindow, width * 95 / 100, ViewGroup.LayoutParams.WRAP_CONTENT, true);
                    popupWindow.setOutsideTouchable(false);
                    popupWindow.setAnimationStyle(R.style.Animation);
                    popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

                    // initialize our XYPlot reference:

                    XYPlot plot = (XYPlot) viewPopupWindow.findViewById(R.id.history_chart);

                    getTotalPlot(plot, mesurementRecords, N, context, totalValues, dateTitles);


                    Slider daySlider = (Slider) viewPopupWindow.findViewById(R.id.days_slider);

                    if (totalValues.size() == 2)
                        {daySlider.setVisibility(view.GONE);}
                    else
                    {
                        daySlider.setValue(Math.min(N, totalValues.size()));
                        daySlider.setValueTo(totalValues.size());
                        daySlider.setValueFrom(2);
                        daySlider.addOnChangeListener(new Slider.OnChangeListener() {
                            @Override
                            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                                int valueInt = Math.round(value);
                                getTotalPlot(plot, mesurementRecords, valueInt, context, totalValues, dateTitles);

                            }
                        });
                    }
                }

            }
        });
        CSV_texte.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                saveToCSV();
            }
        });

        exportCsv.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {
                saveToCSV();
            }
        });

    }



    public void saveToCSV()
    {
        new AlertDialog.Builder(context)
                .setTitle("Souhaitez vous exporter tous les enregistrements précédents ?")
                .setMessage("L'enregistrement se fera au format CSV, et sera enregistré dans le dossier \n \"Android/data/com.example/stressmesurementapp/files/export_csv/export_date.csv\"")
                .setPositiveButton("Oui", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i)
                    {
                        try {
                            String date = getShortDateString();
                            File myExternalFile= new File(getExternalFilesDir("export_csv"),"export_"+date+".csv");
                            // create FileWriter object with file as parameter

                            Writer fileWriter = new FileWriter(myExternalFile);
                            // create CSVWriter object filewriter object as parameter
                            CSVWriter writer = new CSVWriter(fileWriter);

                            // create a List which contains String array
                            List<String[]> data = new ArrayList<String[]>();

                            List<String> allCritList = new ArrayList<>();
                            List<MesurementRecord> mesurementRecords = readMesurementItemListInPref(context);
                            for (MesurementRecord mesurementRecord : mesurementRecords)
                            {
                                for (Map.Entry<String, MesurementItem> entry : mesurementRecord.getMesurements().entrySet()) {
                                    String key = entry.getKey();
                                    MesurementItem item = entry.getValue();
                                    if (!allCritList.contains(item.getId()))
                                    {
                                        allCritList.add(item.getId());
                                    }
                                }
                            }
                            for (MesurementRecord mesurementRecord : mesurementRecords)
                            {
                                List<String> currentRecord = new ArrayList<>();
                                currentRecord.add(mesurementRecord.getTitle());
                                double total = 0;
                                double ponderation=0;
                                for (String crit : allCritList )
                                {
                                    MesurementItem item=mesurementRecord.getMesurements().get(crit);
                                    if (item!=null)
                                    {
                                        Double value = item.getValue() ;
                                        total+=(value / item.getMaxValue()) * 10 * item.getPonderation();
                                        ponderation += item.getPonderation();
                                        Double comp= new Double(1.0);
                                        String pond = new String();
                                        if (! comp.equals(item.getPonderation())) pond = " (x"+String.valueOf(item.getPonderation())+")";
                                        else pond = "";
                                        currentRecord.add(String.valueOf(item.getValue()/item.getMaxValue()*10)+pond);
                                    }
                                    else currentRecord.add("nan");
                                }
                                BigDecimal score = new BigDecimal(total / ponderation).setScale(1, RoundingMode.HALF_UP);
                                currentRecord.add(String.valueOf(score));
                                data.add(currentRecord.toArray(new String[0]));
                            }

                            allCritList.add(0,"Date");
                            allCritList.add("Total");



                            data.add(0,allCritList.toArray(new String[0]));
                            data.add(new String[]{"Lecture : les critères qui n'existent pas dans certains enregistrements seront notés avec 'nan'. Si un critère a une pondération différente de 1 (défaut), elle sera  indiquée entre () avec  un x (par ex (x7.0) pour une pondération de 7). La pondération est prise en compte dans la valeur 'total'."});
                            writer.writeAll(data);

                            // closing writer connection
                            writer.close();

                            Toast.makeText(getApplicationContext(),
                                    "Le fichier a été correctement enregisté a l'adresse" + getExternalFilesDir("export_csv").toString()+"/export_"+date+".csv",
                                    Toast.LENGTH_LONG).show();
                            System.out.println("Le fichier a été correctement enregisté a l'adresse" + getExternalFilesDir("export_csv").toString()+"/export_"+date+".csv");
                        }
                        catch (Exception e) {
                            // TODO Auto-generated catch block
                            Toast.makeText(getApplicationContext(),
                                    "L'enregistrement a échoué ",
                                    Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }



                    }
                }).setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
    }


    public void updateButtonVisibility(Button button_next, Button button_prev,Button deleteTitleButton, List<MesurementRecord> mesurementRecords )
    {
        if (mesurementRecords ==  null)
        {
            hideButton(button_next,true);
            hideButton(button_prev,true);
            hideButton(deleteTitleButton,true);
        }
        else
        {
            if (mesurementRecords.size() == 0) hideButton(deleteTitleButton,true);
            else hideButton(deleteTitleButton,false);

            if (currentDay >= mesurementRecords.size() - 1) //on est sur le dernier jour
            {
                hideButton(button_next,true);
            }
            else hideButton(button_next,false);
            if (currentDay == 0) //premier jour
            {
                hideButton(button_prev,true);
            }
            else hideButton(button_prev,false);
        }
    }

    public void hideButton(Button dayButton, boolean bool)
    {
        if (bool)
        {
            dayButton.setAlpha(0.5F);
            dayButton.setClickable(false);
        }
        else
        {
            dayButton.setAlpha(1F);
            dayButton.setClickable(true);
        }
    }

    public String getDateString()
    {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("EEEE d MMMM , HH:mm:s ", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public String getShortDateString()
    {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("d_MMMM_HH:mm", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public void restoreDay(List<MesurementRecord> mesurementRecords,int dayToRestore )
    {
        LinkedHashMap<String,MesurementItem> itemListNew = mesurementRecords.get(dayToRestore).getMesurements();

        Iterator<String> it = itemList.keySet().iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }


        for (Map.Entry<String,MesurementItem> entry : itemListNew.entrySet() )
        {
            String key = entry.getKey();
            MesurementItem item = entry.getValue();
            MesurementItem newItem=new MesurementItem(item.getText(),mainText,seekbar,item.getValue(),item.getPonderation(),  //NECESSARY cause backup loses transient maintext and seekbar !!
                    item.getMaxValue());
            itemList.put(key,newItem);
        }

        title.setText(mesurementRecords.get(dayToRestore).getTitle());
        mesurements_adapter.notifyDataSetChanged();
        updateButtonVisibility(next_day,previous_day,deleteTitleButton,mesurementRecords);
        updateMainText(itemList,mainText,seekbar);
    }



}