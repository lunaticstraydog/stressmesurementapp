package com.example.stressmesurementapp;

import android.text.Layout;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;

import kotlin.jvm.Transient;

public class MesurementItem {
    private String text;
    private String id;
    private double value;
    private transient TextView mainText; //NOTE transient needed to exclude field from gson
     private transient SeekBar seekbar;

    private int maxValue;

    private double ponderation;

    public MesurementItem(String text,TextView mainText,SeekBar seekbar, double value,double ponderation, int maxValue)
    {
        this.text=text;
        this.id=text;
        this.value=value;
        this.mainText=mainText;
        this.seekbar=seekbar;
        this.maxValue = maxValue;
        this.ponderation=ponderation;
    }
    public MesurementItem(String text,TextView mainText,SeekBar seekbar)
    {
        this.id=text;
        this.text=text;
        this.value=0;
        this.mainText=mainText;
        this.seekbar=seekbar;
        this.maxValue=10;
        this.ponderation=1.0;
    }

    public double getValue() {
        return value;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public double getPonderation() {
        return ponderation;
    }

    public void setMaxValue(int value) {
        maxValue=value;
    }

    public void setPonderation(double ponderation) {
        this.ponderation=ponderation;
    }
    public TextView  getMainText() {
        return mainText;
    }

    public void setValue(double value) {this.value=value; return ;}


    public String getText() {
        return text;
    }
    public String getId() {
        return id;
    }


    public SeekBar getSeekBar() {
        return seekbar;
    }

    public void setText(String text) {
        this.text=text;
    }

    public void setMainText(TextView mainText) {this.mainText = mainText;}

    public void setSeekBar(SeekBar seekbar) {this.seekbar=seekbar;}

}
