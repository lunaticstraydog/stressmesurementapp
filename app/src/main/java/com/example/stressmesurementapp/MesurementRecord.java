package com.example.stressmesurementapp;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class MesurementRecord {
    private LinkedHashMap<String,MesurementItem> mesurements;
    private String title;

    public Double total;
    public MesurementRecord(LinkedHashMap<String,MesurementItem> mesurements, String title) {
        this.mesurements = mesurements;
        this.title = title;
    }

    public MesurementRecord() {
        this.mesurements = new LinkedHashMap<>();
        this.title = "placeholder title";
    }

    public String getTitle() {
        return this.title;
    }

    public LinkedHashMap<String,MesurementItem> getMesurements() {
        return mesurements;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getTotal() {
        return total;
    }

    public void setMesurements(LinkedHashMap<String,MesurementItem> mesurements) {
        this.mesurements = mesurements;
    }
}
