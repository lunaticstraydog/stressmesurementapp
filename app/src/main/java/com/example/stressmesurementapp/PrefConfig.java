package com.example.stressmesurementapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class PrefConfig {
    private static final String LIST_KEY = "list_key";
    private static final String NEW_TUPLE_KEY = "tuple_key_-112"; //-109



    public static void writeListInPref(Context context, List<String> liste)
    {
        Gson gson = new Gson();
        String jsonString=gson.toJson(liste);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor =pref.edit();
        editor.putString(LIST_KEY,jsonString);
        editor.apply();
    }


    public static List<String> readListInPref(Context context)
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String jsonString=pref.getString(LIST_KEY, "");
        Gson gson = new Gson();
        Type type=new TypeToken<ArrayList<String>>() {}.getType();
        List<String> liste=gson.fromJson(jsonString, type );
        return liste;
    }


    public static void writeMesurementItemListInPref(Context context, List<MesurementRecord> mesurementRecords)
    {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        String jsonStringValueList=gson.toJson(mesurementRecords);
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor =pref.edit();
        editor.putString(NEW_TUPLE_KEY,jsonStringValueList);
        editor.apply();
    }


    public static List<MesurementRecord> readMesurementItemListInPref(Context context)
    {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        String jsonStringValueList=pref.getString(NEW_TUPLE_KEY, "");
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Type type_tuple=new TypeToken<List<MesurementRecord>>() {}.getType();
        List<MesurementRecord> mesurementRecords=gson.fromJson(jsonStringValueList,type_tuple );
        return mesurementRecords;
    }



}
